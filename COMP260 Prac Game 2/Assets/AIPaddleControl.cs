﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPaddleControl : MonoBehaviour {

	private Rigidbody rigidbody;
	public float vel = 10.0f;
	public GameObject puck;
	public GameObject goal;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 midpoint = (goal.transform.position + puck.transform.position) / 2;
		transform.position = midpoint;
	}
}
