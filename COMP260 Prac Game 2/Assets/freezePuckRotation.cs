﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class freezePuckRotation : MonoBehaviour {
	private Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		rigidbody.freezeRotation = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
