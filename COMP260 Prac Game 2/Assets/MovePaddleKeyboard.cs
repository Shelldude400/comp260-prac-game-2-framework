﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour {
	public float speed = 20.0f;
	private Rigidbody rigidbody;

	void Start() {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		rigidbody.freezeRotation = true;
	}
	void Update() {
	
	}

	void FixedUpdate() {
		// get the input values
		Vector3 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
		direction.z = 0;

		// scale by the maxSpeed parameter
		Vector3 vel = direction.normalized * speed;

		// move the object
		rigidbody.velocity = vel;
	}
}
